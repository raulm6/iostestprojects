//
//  ViewController.swift
//  IsItPrime
//
//  Created by Raul Monraz on 11/18/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func isPrime(num : Int) -> Bool {
        
        if (num == 1 || num < 1) { return false }
        if (num == 2 || num == 3){ return true }
        
        let numRoot = Int(sqrt(Double(num)))
        
        for i in 2...numRoot{
            if (num % i) == 0 {
                return false
            }
        }
        
        return true
    }

    @IBAction func buttonPressed(_ sender: Any) {
        
        if let userInput = input.text, let userNum = Int(userInput) {
            result.text = (isPrime(num : userNum) ? "PRIME" : "NOT PRIME")
        } else {
            result.text = "Invalid Input"
        }
        
    }
    @IBOutlet weak var result: UILabel!
    @IBOutlet weak var input: UITextField!
    
}

extension UIViewController
{
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
}
