//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

var my = "Hello"

var newString = my + " Raul!"

for character in newString.characters {
    print(character)
}

let newTypeString = NSString(string: newString)

NSString(string: newTypeString.substring(from: 6)).substring(to: 4)

newTypeString.substring(with: NSRange(location: 6, length: 4))

if newTypeString.contains("Raul") {
    print("Hell yeah!")
}

newTypeString.components(separatedBy: " ")

newTypeString.uppercased
