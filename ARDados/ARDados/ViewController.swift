//
//  ViewController.swift
//  ARDados
//
//  Created by Raul Monraz on 10/28/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    var diceArray = [SCNNode]()
    
    @IBOutlet var sceneView: ARSCNView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
        // Set the view's delegate
        sceneView.delegate = self
        
        
//        let cube = SCNBox(width: 0.1, height: 0.1, length: 0.1, chamferRadius: 0.01)
//        let material = SCNMaterial()
//        material.diffuse.contents = UIColor.red
//        cube.materials = [material]
//
//        let node = SCNNode()
//        node.position = SCNVector3(x: 0, y: 0.05, z: -0.5)
//        node.geometry = cube
//
//        let sphere = SCNSphere(radius: 0.2)
//        let sphere_material = SCNMaterial()
//        sphere_material.diffuse.contents = UIImage(named: "art.scnassets/mars.jpg")
//        sphere.materials = [sphere_material]
//
//        let snode = SCNNode()
//        snode.position = SCNVector3(x: -0.5, y: 0.05, z: -0.5)
//        snode.geometry = sphere
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true

//        sceneView.scene.rootNode.addChildNode(node)
//        sceneView.scene.rootNode.addChildNode(snode)
        sceneView.autoenablesDefaultLighting = true
        
        // Create a new scene
//        let diceScene = SCNScene(named: "art.scnassets/diceCollada.scn")!
//        if let diceNode = diceScene.rootNode.childNode(withName: "Dice", recursively: true) {
//            diceNode.position = SCNVector3(x: 0, y: 0, z: -0.1)
//            sceneView.scene.rootNode.addChildNode(diceNode)
//        }
//
        
        // Set the scene to the view
        
        //sceneView.scene = scene
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let touchLocation = touch.location(in: sceneView)
            let results = sceneView.hitTest(touchLocation, types: .existingPlaneUsingExtent)
            
//            if !results.isEmpty {
//                print("Touched Plane!")
//
//            } else {
//                print("Didn't touch anything!")
//            }
            
                if let hitResult = results.first {
                    //print(hitResult)
                    addDice(atLocation: hitResult)
                }
            }
    }
    
    func addDice(atLocation location : ARHitTestResult) {
            let diceScene = SCNScene(named: "art.scnassets/diceCollada.scn")!
            if let diceNode = diceScene.rootNode.childNode(withName: "Dice", recursively: true) {
                diceNode.position = SCNVector3(
                    x: location.worldTransform.columns.3.x,
                    y: location.worldTransform.columns.3.y + diceNode.boundingSphere.radius,
                    z: location.worldTransform.columns.3.z)
                
                diceArray.append(diceNode)
                
                sceneView.scene.rootNode.addChildNode(diceNode)
                roll(dice: diceNode)
        }
    }
    
    func rollAll() {
        if !diceArray.isEmpty {
            for dice in diceArray{
                roll(dice: dice)
            }
        }
    }
    func removeAll() {
        if !diceArray.isEmpty {
            for dice in diceArray{
                dice.removeFromParentNode()
            }
        }
    }
    
    func roll(dice : SCNNode) {
        let randomX = Float(arc4random_uniform(4) + 1) * (Float.pi/2)
        let randomZ = Float(arc4random_uniform(4) + 1) * (Float.pi/2)
        dice.runAction(SCNAction.rotateBy(x: CGFloat(randomX * 5), y: 0, z: CGFloat(randomZ * 5), duration: 0.5))
    }
    
    @IBAction func removeAllDice(_ sender: UIBarButtonItem) {
        removeAll()
        
        
    }
    @IBAction func rollAgain(_ sender: UIBarButtonItem) {
        rollAll()
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        rollAll()
    }
    
    //MARK: = ARSCNViewDelegateMethods
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        
        guard let planeAnchor = anchor as? ARPlaneAnchor else {return}
        node.addChildNode(createPlane(withPlaneAnchor: planeAnchor))
        
//        if anchor is ARPlaneAnchor {
//
//        } else {
//            return
//        }
    }
    
    //MARK: - Plane Rendering Methods
    func createPlane(withPlaneAnchor planeAnchor: ARPlaneAnchor) -> SCNNode {
        print("Plane Detected")
        let plane = SCNPlane(width: CGFloat(planeAnchor.extent.x), height: CGFloat(planeAnchor.extent.z))
        let planeNode = SCNNode()
        planeNode.position = SCNVector3(x: planeAnchor.center.x, y: 0, z: planeAnchor.center.z)
        planeNode.transform = SCNMatrix4MakeRotation(-Float.pi/2, 1, 0, 0)
        
        let gridMaterial = SCNMaterial()
        gridMaterial.diffuse.contents = UIImage(named: "art.scnassets/grid.png")
        plane.materials = [gridMaterial]
        
        planeNode.geometry = plane
        return planeNode
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    // MARK: - ARSCNViewDelegate
    
/*
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
     
        return node
    }
*/
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}

//print("Plane Detected")
//let planeAnchor = anchor as! ARPlaneAnchor
//let plane = SCNPlane(width: CGFloat(planeAnchor.extent.x), height: CGFloat(planeAnchor.extent.z))
//let planeNode = SCNNode()
//planeNode.position = SCNVector3(x: planeAnchor.center.x, y: 0, z: planeAnchor.center.z)
//planeNode.transform = SCNMatrix4MakeRotation(-Float.pi/2, 1, 0, 0)
//
//let gridMaterial = SCNMaterial()
//gridMaterial.diffuse.contents = UIImage(named: "art.scnassets/grid.png")
//plane.materials = [gridMaterial]
//
//planeNode.geometry = plane
//node.addChildNode(planeNode)

