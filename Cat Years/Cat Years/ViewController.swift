//
//  ViewController.swift
//  Cat Years
//
//  Created by Raul Monraz on 11/17/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  
    @IBOutlet weak var input: UITextField!
    @IBOutlet weak var result: UILabel!
    
    @IBAction func buttonPressed(_ sender: Any) {
        if let inputText = input.text, let humanYears = Int(inputText)  {
            result.text = "You would be \(String(format: "%.2f", Double(humanYears) / 7))"
            
        } else {
            result.text = "Invalid Input"
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.hideKeyboard()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension UIViewController
{
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
}
