//
//  ViewController.swift
//  WhatIsMyNumber
//
//  Created by Raul Monraz on 11/17/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var result: UILabel!
    
    @IBOutlet weak var input: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if let savedNumber = UserDefaults.standard.object(forKey: "userNumber"), let
            savedNumberString = savedNumber as? String {
            result.text = savedNumberString
        } else {
            result.text = "No Number Saved Yet"
        }
        
    }
    @IBOutlet weak var saveNumber: UIButton!
    
    @IBAction func numberSaved(_ sender: Any) {
        
        if let number = input.text {
            UserDefaults.standard.set(number, forKey: "userNumber")            
            result.text = number
            
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

