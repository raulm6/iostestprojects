//
//  ViewController.swift
//  WhatsTheWeather
//
//  Created by Raul Monraz on 11/20/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textfield: UITextField!
    @IBOutlet weak var result: UILabel!
    
    @IBAction func getWeatherPressed(_ sender: Any) {
        print("pressed")
        let url = URL(string: "http://www.weather-forecast.com/locations/London/forecasts/latest")!
        let request = NSMutableURLRequest(url: url)
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            if error != nil {
                print("error!")
            } else {
                if let unwrappedData = data {
                    let dataString = NSString(data: unwrappedData, encoding: String.Encoding.utf8.rawValue)
                    //print(dataString!)
                    
                    let stringSeparator = "Weather Forecast Summary:"
                    if let contentArray = dataString?.components(separatedBy: stringSeparator) {
                        print(contentArray)
                    }
                    
                }
            }
        }
        task.resume()
        
        
        result.text = "pressed"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

}

