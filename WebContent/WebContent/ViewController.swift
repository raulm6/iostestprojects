//
//  ViewController.swift
//  WebContent
//
//  Created by Raul Monraz on 11/20/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKUIDelegate {

    
    
    @IBOutlet weak var emptyView: UIView!
    
    var webView : WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       
        
    }

    override func viewDidAppear(_ animated: Bool) {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: self.emptyView.bounds, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.bounds = emptyView.bounds
        emptyView.addSubview(webView)
        let myURL = URL(string: "https://www.apple.com")
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

/*
 var webView: WKWebView!
 
 override func loadView() {
 let webConfiguration = WKWebViewConfiguration()
 webView = WKWebView(frame: .zero, configuration: webConfiguration)
 webView.uiDelegate = self
 view = webView
 }
 override func viewDidLoad() {
 super.viewDidLoad()
 
 let myURL = URL(string: "https://www.apple.com")
 let myRequest = URLRequest(url: myURL!)
 webView.load(myRequest)
 }}
 */

