# README #

This is a repository containing several iOS applications that were created throughout the semester to learn the specifics
of iOS development and the utilization of the ARKit, CoreData and Cocoapods Frameworks.

### What is this repository for? ###

This repository is meant to accompany the deliverables for the project
