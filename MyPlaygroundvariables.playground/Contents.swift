//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

let num1 = 5.76
let num2 = 8
print(String(format: "the product of %f and %d is: %f", num1, num2, num1 * Double(num2)))

var myArray = [3,87, 7.1, 8.9]
myArray.remove(at: 1)
myArray.append(myArray[0] * myArray[1])
print(myArray)

let mixArray = ["Rob", 35, true] as [Any]

// Dictionary

let dictionary = ["computer": "something to play Call of Duty on", "coffee": "best Drink Ever"]
print(dictionary["computer"]!)

//

var menu = [String : Decimal]()
menu = ["pizza": 10.99, "ice cream": 4.99, "salad":7.99]

var sum : Decimal = 0
menu.forEach {
    print($0.value)
    sum += $0.value
    
}
print(sum)

print(arc4random_uniform(5))

