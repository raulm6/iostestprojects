//
//  ViewController.swift
//  HowManyFingers
//
//  Created by Raul Monraz on 11/17/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var input: UITextField!
    @IBOutlet weak var result: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.hideKeyboard()
    }

    func guessNumber(_ userguess : Int ) -> Bool {
        return userguess == arc4random_uniform(5) + 1
    }
    
    @IBAction func guessButton(_ sender: Any) {
        
        if let numberText = input.text,
            let number = Int(numberText) {
            let guessed = guessNumber(number)
            result.text = guessed ? "You Guessed!" : "Wrong guess!"
        } else {
            result.text = "Invalid Input"
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension UIViewController
{
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
}
