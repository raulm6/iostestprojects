//
//  SelfDrivingCar.swift
//  Classes and Objects
//
//  Created by Raul Monraz on 10/26/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import Foundation

class SelfDrivingCar : Car {
    
    var destination : String = "1 Infinite Loop"
    
    override func drive() {
        super.drive()
        print("driving towards " + destination)
    }
    
}
