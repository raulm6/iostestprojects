//
//  main.swift
//  Classes and Objects
//
//  Created by Raul Monraz on 10/26/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import Foundation

print("Hello, World!")

var myCar = Car()

print(myCar.carType)
print(myCar.colour)
print(myCar.numberOfSeats)

myCar.drive()

let mySelfDrivingCar = SelfDrivingCar()

mySelfDrivingCar.drive()
