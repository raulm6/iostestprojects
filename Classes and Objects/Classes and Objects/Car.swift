//
//  File.swift
//  Classes and Objects
//
//  Created by Raul Monraz on 10/26/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import Foundation

enum CarType {
    case Sedan
    case Coupe
    case Hatchback
}

class Car {
    var colour : String = "Black"
    var numberOfSeats : Int = 5
    var carType : CarType = .Coupe
    
    init(){
        
    }
    
    convenience init(chosenColor : String ) {
        self.init()
        colour = chosenColor
    }
    
    func drive() {
        print("my car is moving")
    }
    
}
