//
//  ViewController.swift
//  IntroCoreData
//
//  Created by Raul Monraz on 11/21/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var userTextField: UITextField!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var logButton: UIButton!
    
    var loggedIn : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        
        request.returnsObjectsAsFaults = false
        
        do {
        
            let results = try context.fetch(request)
            for result in results as! [NSManagedObject] {
                if let username = result.value(forKey: "name") as! String? {
                    welcomeUser(username)
                }
            }
            
            
        } catch {
            
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func welcomeUser(_ username: String) {
        messageLabel.text = "Hi there \(username)!"
        messageLabel.alpha = 1
        userTextField.alpha = 0
        logButton.alpha = 0
    }

    @IBAction func loginPressed(_ sender: UIButton) {
        if let username = userTextField.text, username.count > 0 {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            
            let newValue = NSEntityDescription.insertNewObject(forEntityName:"Users", into: context)
            
            newValue.setValue(username, forKey: "name")
            
            do {
                
                try context.save()
                welcomeUser(username)
            } catch {
                print("failed to save")
            }
            
            
            
            
            
        }
        
    }
    
}

