//
//  ViewController.swift
//  My First App Percival
//
//  Created by Raul Monraz on 11/17/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textfield: UITextField!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var clickMeButton: UIButton!
    
    @IBAction func buttonClicked(_ sender: Any) {
            print("button clicked")
        
        if let name = textfield.text, name.count > 0 {
            label.text = "Hi  \(name)!"
        } else {
            label.text = "Hi Anonymous!"
        }
        
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        print("Raul")
        self.hideKeyboard()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension UIViewController
{
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
}

