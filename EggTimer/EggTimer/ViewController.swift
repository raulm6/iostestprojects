//
//  ViewController.swift
//  EggTimer
//
//  Created by Raul Monraz on 11/20/17.
//  Copyright © 2017 Raul Monraz. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var timer = Timer()
    var time = 1000
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        timerDisplay.text = "\(time)"
        
    }

    @IBOutlet weak var timerDisplay: UILabel!

    @IBAction func playPressed(_ sender: Any) {
        timer.invalidate()
                timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ViewController.processTimer), userInfo: nil, repeats: true)
    }

    @IBAction func pausePressed(_ sender: Any) {
        timer.invalidate()
    }
    
    @IBAction func resetPressed(_ sender: Any) {
        timer.invalidate()
        time = 1000
        timerDisplay.text = "\(time)"
    }

    @IBAction func minusTen(_ sender: Any) {
        if (time >= 10) {
            time -= 10
            timerDisplay.text = "\(time)"
        }
        
    }

    @IBAction func addTen(_ sender: Any) {
        time += 10
        timerDisplay.text = "\(time)"
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @objc func processTimer() {
        if (time > 0){
            time = time - 1
        } else {
            timer.invalidate()
        }
        timerDisplay.text = "\(time)"
    }

}

